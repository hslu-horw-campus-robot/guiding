# guiding

[[_TOC_]]

## Status
- compiles on go1
- is started during autostart of go1 nx board (check for node "goal_request_translator")
- Goals: check the currently available options in the code (request_translator.cpp) for now.
- NOTE that "goals" can be sent from go1 webui (click corresponding button, check debug output in the lower section), from control_states packages (monitor with `rostopic echo \ui_request` and `rostopic echo \goto_goal`) or from terminal (`rostopic pub \ui_request ...` or `rostopic pub \goto_goal ...`) 

### Debugging
- map can be accessed with go1 mobile phone app (for debugging)
- local goals can also be set with the mobile phone app

### TODO
Implement ROS code to reach more locations.

Coordinates measured in map from location (0/0):
| Location | pos x | pos y | orient z|orient w|
| ------ | ------ |------ |------|------ |
| Türe innen Gebäude 2 Stock D |  2.6      |1.4|
| Türe aussen       |   5.2     |1.3| 1  |0 |
| Türe aussen Gebäude 5 Stock D (Mensa)|11.03|-20.3|-0.29|0.957|
| äussere Türe innen Gebäude 5 Stock D|13.43|-20.76|1|0|
| innere Türe aussen Gebäude 5 Stock D|15.3|-20.3|||
| innere Türe innen|17.59|-20.56|0.991|0.128|
| Eingang Mensa|30|-23.25|-0.46|0.88|
| Eingang Bibliothek|32.53|-19.7|0.347|0.937|

## Description
Contains code to use go1 for guiding to specific location. ROS counterpart of Go1 web UI.

## Installation
Like other ROS packages / nodes.

1. create a ROS workspace, e.g. ```mkdir hslu_ws/src```
2. cd hslu_ws
3. catkin_make
4. clone (or copy if not internet connection) guiding into /src folder
5. catkin_make

## Usage
1. Start node with ```rosrun guiding request_translator```
2. in another terminal, enter the goal e.g. ```rostopic pub /ui_request std_msgs/String "canteen"```

### Test goals / (emergency) stop
For testing the goals "home" (0/0), "test" (1/0), and "halt (current x/current y) have been implemented. (They are mapped to buttons on webui, too.)

