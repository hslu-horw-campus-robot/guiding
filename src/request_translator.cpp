#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <cmath>


typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

geometry_msgs::PoseStamped poseSt;
geometry_msgs::PoseStamped odomPose;
bool new_request = false;
bool monitor_request = false;
int counter = 0;

// coordinates of canteen and library have been measured in the map
const float HOME_X = 0.0;
const float HOME_Y = 0.0;
const float CANT_X = 30;
const float CANT_Y = -23.3;
const float LIB_X = 32.5;
const float LIB_Y = -19.7;
const float WING2_X = 5.2;
const float WING2_Y = 1.3;
const float WING5_X = 11.0;
const float WING5_Y = -20.3;
const float TEST_X = 1.0;
const float TEST_Y = 0.0;

/* keeping track of current location */
void odomCallback(const nav_msgs::Odometry::ConstPtr& msg) {
   odomPose.pose.position.x = msg->pose.pose.position.x;
   odomPose.pose.position.y = msg->pose.pose.position.y;
   odomPose.pose.position.z = 0;
   odomPose.pose.orientation.x = 0;
   odomPose.pose.orientation.y = 0;
   odomPose.pose.orientation.z = msg->pose.pose.orientation.z;
   odomPose.pose.orientation.w = msg->pose.pose.orientation.w;
   counter++;
   if (counter%20 == 0) {
       ROS_INFO("tracking updated");
   }
}

const float DIST_LIM = 0.6; // distance limit in meters

float eucl_dist(const float p1, const float p2, const float q1, const float q2)
{
    float x_dist = (q1-p1)*(q1-p1);
    float y_dist = (q2-p2)*(q2-p2);
    float distance = std::sqrt(x_dist+y_dist);
    return distance;
}

void gotogoalCallback(const std_msgs::String::ConstPtr& msg) {
    if (msg->data == "canteen"){
        /* compiles, but not tested:
        guiding::SetNextWaypose::Request req;
        guiding::SetNextWaypose::Response res;
        req.x_m = 1.0;
        req.y_m = 0.0;
        req.angle_rad = 0.0;
        ros::ServiceClient waypoint_service;
        waypoint_service.call(req, res);
        */
        // variant from the third link, publishing move_base_simple/goal
	    ROS_INFO("Requested location: canteen");
        poseSt.pose.position.x = CANT_X;
        poseSt.pose.position.y = CANT_Y;
        poseSt.pose.orientation.z = -0.46;
        poseSt.pose.orientation.w = 0.88;
        new_request = true;
    } else if (msg->data == "library"){
	    ROS_INFO("Requested location: library");
        poseSt.pose.position.x = LIB_X;
        poseSt.pose.position.y = LIB_Y;
        poseSt.pose.orientation.z = 0.347;
        poseSt.pose.orientation.w = 0.937;
	    new_request = true;
    } else if (msg->data == "home"){
	    ROS_INFO("Requested location: home");
        poseSt.pose.position.x = HOME_X;
        poseSt.pose.position.y = HOME_Y;
        poseSt.pose.orientation.z = 0;
        poseSt.pose.orientation.w = 1;
	    new_request = true;
    } else if (msg->data == "wing2"){
	    ROS_INFO("Requested location: wing2");
        poseSt.pose.position.x = WING2_X;
        poseSt.pose.position.y = WING2_Y;
        poseSt.pose.orientation.z = 1;
        poseSt.pose.orientation.w = 0;
	    new_request = true;
    } else if (msg->data == "wing5"){
	    ROS_INFO("Requested location: wing5");
        poseSt.pose.position.x = WING5_X;
        poseSt.pose.position.y = WING5_Y;
        poseSt.pose.orientation.z = -0.29;
        poseSt.pose.orientation.w = 0.957;
	    new_request = true;
    } else if (msg->data == "test"){
	    ROS_INFO("Requested location: test");
        poseSt.pose.position.x = TEST_X;
        poseSt.pose.position.y = TEST_Y;
        poseSt.pose.orientation.z = 0;
        poseSt.pose.orientation.w = 1;
	    new_request = true;
    } else if (msg->data == "halt"){
	    ROS_INFO("Requested location: HALT");
        poseSt.pose.position.x = odomPose.pose.position.x;
        poseSt.pose.position.y = odomPose.pose.position.y;
        poseSt.pose.orientation.z = odomPose.pose.orientation.z;
        poseSt.pose.orientation.w = odomPose.pose.orientation.w;
	    new_request = true;
    }else {
        ROS_INFO("Unhandled UI request: [%s]", msg->data.c_str());
    }
}


/* This node subscribes to the topic(s) published by the web UI (and received via ROS bridge).
 * It translates a web UI request to an absolute goal on the internal map representation of Go1.
 * It then calls the service request for the computed goal.
 */
int main(int argc, char **argv)
{
  ROS_INFO("goal_request_translator starting ...");
  ros::init(argc, argv, "goal_request_translator");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("goto_goal", 10, gotogoalCallback);
  ros::Subscriber sub_o = n.subscribe("odom", 10, odomCallback);
  ros::Publisher goal_pub = n.advertise<geometry_msgs::PoseStamped>("move_base_simple/goal", 10);
  ros::Publisher reached_pub = n.advertise<geometry_msgs::PoseStamped>("guiding/reached", 10);
  ros::Rate loop_rate(2);

  // init common pose values
  poseSt.header.frame_id = "map";
  poseSt.pose.position.z = 0.0;
  poseSt.pose.orientation.x = 0;
  poseSt.pose.orientation.y = 0;

  while(ros::ok())
  {
      if(new_request){
          goal_pub.publish(poseSt);
          new_request = false;
          monitor_request = true;
	      ROS_INFO("new pose published: x=[%f], y=[%f], current pos: x=[%f], y=[%f]", poseSt.pose.position.x, poseSt.pose.position.y, odomPose.pose.position.x, odomPose.pose.position.y);
      }
      // check if go1 has come close to required pose. if so, let state machine know.
      float current_dist = eucl_dist(poseSt.pose.position.x, poseSt.pose.position.y, odomPose.pose.position.x, odomPose.pose.position.y);
      ROS_INFO("current dist: [%f]", current_dist);
      // monitor only active/new requests:
      if ((current_dist <= DIST_LIM) && (monitor_request == true)){
          reached_pub.publish(odomPose);
          monitor_request = false;
      }
      ros::spinOnce();
      loop_rate.sleep();
  }
  return 0;
}
